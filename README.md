# Nginx limiting the request rate
## A - Sử dụng để làm gì ?
- ngăn chặn các cuộc tấn công DDos
- ngăn chặn các upstream server bị overwhelmed (choáng ngợp) khi có quá nhiều request cùng một thời điểm
## B - Cơ chế hoạt động ?
![alt text](images/image1.png)
- dựa trên thuật toán Leaky bucket : thuật toán này hoạt động tương tự như việc chúng ta có 1 cái xô (bucket), nước được đổ vào xô từ trên xuống và chảy ra ngoài (leak) bởi những cái lỗ ở đáy xô với tốc độ cố định. Nếu tốc độ đổ nước vào xô (incoming rate) cao hơn tốc độ nước chảy ra ngoài (outgoing rate) thì đến 1 lúc nào đó, nước trong xô sẽ đầy và tràn ra ngoài (overflow)
- Trong ngữ cảnh của xử lý request:
  + “Nước” tượng trưng cho request của client
  + “Xô” tượng trưng cho queue, nơi chứa các request đang đợi được xử lý, queue hoạt động theo thuật toán FIFO (First In – First Out), request nào đến trước sẽ được xử lý trước.
  + “Nước chảy ra ở đáy xô” (leaking water) tượng trưng cho các request được lấy ra khỏi queue để server xử lý, tốc độ chảy ra của nước tượng trưng cho tần số (rate) mà ta quy định. Nếu nước đổ vào xô quá nhanh (client request quá nhanh) khiến xô bị đầy (full queue), phần nước chảy ra ngoài (overflow) tượng trưng cho các request sẽ bị lọai bỏ (discarded) và không bao giờ được phục vụ.

## C - Cấu hình
- Các tham số chung trong cấu hình leaky bucket
  + key : tham số phân biệt giữa client này với client khác, với các biến như : $binary_remote_addr (lưu trữ địa chỉ IP dưới dạng binary , sử dụng ít bộ nhớ hơn biến $remote_addr), $remote_addr  => tính toán tần số (rate) và apply limit
  + zone (shared memory zone) : tên và kích thước của zone giữ trạng tháu của các key này => lưu trữ trạng thái của từng IP và tần số truy cập của chúng vào các URL đã apply limit
    VD : zone=zone_name:10m (zone_name: tên của zone, 10m: kích thước của zone, 16000 IP ~ 1MB bộ nhớ)
  + rate : giới hạn tần số request tối đa được chỉ định trong requests pre second (r/s) hoặc requests per minute (r/m) 
  
- Các key directive
  + limit_req_zone : thiết lập vùng bộ nhớ dùng chung để lưu trữ thông tin rate limit , được cấu hình nằm trong http{} => xác định tần số các yêu cầu được phép , thiết lập hiệu quả baseline cho rate limit 
  + limit_req : được cấu hình năm trong location{}  => thực thi limit_req_zone , áp dụng các giới hạn này cho các loaction hoặc URL cụ thể => kiểm soát được tần suất truy cập và các endpoint
  + limit_req_status : được cấu hình năm trong location{} , chỉ định HTTP status code được trả về khi request rate vượt quá giới hạn => cơ chế phản hồi này rất quan trọng để thông báo cho người dùng và hệ thống tự động khi chúng vượt quá tần suất request được phép

### 1 - Cấu hình giới hạn tần số (rate) Nginx
a - Cấu hình
```
    $ sudo vim /etc/nginx/nginx.conf
```
```bash
    http {
        limit_req_zone $binary_remote_addr zone=mylimit:10m rate=1r/s;
 
        server {
            listen 80;
            server_name wss.vn;

            location / {
                    limit_req zone=mylimit;
                    proxy_pass http://backend;
            }
        }
    }
```
b - Test
```
    $ sudo vim curlrequest.sh
```
```bash
    curl http://wss.vn/ &
    curl http://wss.vn/
```
```
    $ sudo bash curlrequest.sh
```
![alt text](images/image2.png)

c - Giải thích
- mỗi IP sẽ bị giới hạn tối đa 1 request mỗi giây cho /, hoặc chính xác hơn, không thể truy cập vào URL trên trong khoảng 100ms so với request trước đó.

### 2 - Cấu hình xử lý nhiều truy cập hợp lệ đến cùng lúc (bursts)
a - Cấu hình
```
    $ sudo vim /etc/nginx/nginx.conf
```
```bash
    http {
 
        limit_req_zone $binary_remote_addr zone=mylimit:10m rate=1r/s;
 
        server {
            listen 80;
            server_name wss.vn;

            location / {
                    limit_req zone=mylimit burst=5;
                    proxy_pass http://backend;
            }
        }
    }
```
b - Test
```
    $ sudo vim curlrequest.sh
```
```bash
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/
```
```
    $ sudo bash curlrequest.sh
```
![alt text](images/image3.png)

c - Giải thích
- giả sử có 2 request cùng lúc => request đầu tiên sẽ được chấp nhận và request thứ 2 sẽ được trả về lỗi 503 ==> chúng ta sẽ buffer những request đến sớm hơn giới hạn và phục vụ chúng sau đó => sử dụng tham số burst cho limit_req
- giới hạn tốc độ (rate) là 1r/s , giới hạn burst (queue size) là 5 request => nếu 6 request đến cùng lúc từ 1 IP => Nginx sẽ forward request đầu tiên đến upstream server group ngya lập tức và put 5 request còn lại vào queue và sau đó nó forward từng request trong queue trong mỗi 10ms , và sẽ return về 503 cho client chỉ khi số lượng request trong queue vượt quá 5
- Log : request đầu tiên trong log từ địa chỉ IP 192.168.188.1 là request thứ 6 trong vòng 8 giây (mỗi req mỗi giây) => nó vượt quá giới hạn burst và NGINX trả lại lỗi 503, các request sau đó (từ request thứ 2 đến request thứ 7) đều nằm trong giới hạn tốc độ (1 yêu cầu mỗi giây) nên được xử lý thành công với mã trạng thái 200 OK.
### 3 - Xử lý hàng đợi mà ko delay request
a - Cấu hình
```
    $ sudo vim /etc/nginx/nginx.conf
```
```bash
    http {
 
        limit_req_zone $binary_remote_addr zone=mylimit:10m rate=1r/s;
 
        server {
            listen 80;
            server_name wss.vn;


            location / {
                    limit_req zone=mylimit burst=5 nodelay;
                    proxy_pass http://backend;
            }
        }
    }
```
b - Test
```
    $ sudo vim curlrequest.sh
```
```bash
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/
```
```
    $ sudo bash curlrequest.sh
    $ sudo bash curlrequest.sh
```
![alt text](images/image4.png)

c - Giải thích
- với tham số nodelay => Nginx sẽ cấp (allocate) slots cho request ở trong queue theo đúng cấu hình của tham số burst và áp đặt rate limit, nhưng nó không gian cách (không delay) việc forward các request ở trong queue
- khi request đến quá sớm, Nginx forward request đến upstream application ngay lập tức, miễn là trong queue vẫn còn slot => NGINX sẽ đánh dấu (mark) slot đó là “đã sử dụng” (taken) và không cho các request khác sử dụng slot này cho đến khi thời gian tương ứng trôi qua
- Log : ta đang có 5 slot trống trong queue và nhận được 6 request đến đồng thời từ một địa chỉ IP. NGINX sẽ forward 6 request này ngay lập tức và đánh dấu 5 slot trong queue là “đã dùng”, sau đó, NGINX sẽ giải phóng (free) 1 slot sau mỗi 10ms , Giả sử bây giờ là 11ms sau khi mớ request đầu tiên được forward và tiếp tục nhận được 6 request mới đến cùng lúc. Chỉ có 1 slot vừa được giải phóng, vì vậy, NGINX sẽ forward 1 request và reject 5 request còn lại với tatus code 503.

### 4 - Cấu hình giới hạn tần số 2 tầng (two-stage rate limiting)
a - Cấu hình
```
    $ sudo vim /etc/nginx/nginx.conf
```
```bash
    http {
 
        limit_req_zone $binary_remote_addr zone=mylimit:10m rate=1r/s;
 
        server {
            listen 80;
            server_name wss.vn;
            
            location / {
                    limit_req zone=mylimit burst=5 delay=3;
                    proxy_pass http://backend;
            }
        }
    }
```
b - Test
```
    $ sudo vim curlrequest.sh
```
```bash
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/ &
    curl http://wss.vn/
```
```
    $ sudo bash curlrequest.sh
```
![alt text](images/image5.png)
c - Giải thích
- Two-stage limiting được kích hoạt bằng cách khai báo tham số delay trong limit_req directive => Two-stage limiting được giải thích như sau : cấu hình NGINX cho phép burst request để thích nghi với đặc điểm truy cập (request pattern) của các browser thông thường, và sau đó giới hạn những request đã vượt qua một ngưỡng ( ngưỡng A) nào đó, và nếu vượt trên một ngưỡng trên nữa (ngưỡng B), request sẽ bị reject
- Cấu hình cho phép burst tối đa 5 request, 3 request đầu tiên sẽ được xử lý ngay lập tức mà không bị delay. Delay sẽ được apply sau request thứ 3 để đảm bảo tần số 1 request/s. Sau 5 excessive requests, những request sau đó sẽ bị reject
- Ví dụ : rate=5r/s , burst=12 và delay=8
![alt text](images/image6.png)
=> 8 request đầu tiên (bằng giá trị khai báo của tham số delay) được xử lý mà không bị delay. 4 request tiếp theo (burst – delay) sẽ bị delay và tần số 5 request/s không bị vượt quá. 3 request tiếp theo bị reject vì vượt quá burst size. Các request sau đó cũng bị delay

## D - Perferences
- https://vietnix.vn/huong-dan-cau-hinh-nginx-rate-limit/
- https://docs.nginx.com/nginx/admin-guide/security-controls/controlling-access-proxied-http/
- https://www.linuxcapable.com/how-to-rate-limit-in-nginx/
